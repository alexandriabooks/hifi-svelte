import { ApolloClient, InMemoryCache } from "@apollo/client/core"

export const client = new ApolloClient({
    uri: '/api/graphql',
    cache: new InMemoryCache()
});
